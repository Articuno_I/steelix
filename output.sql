SELECT
    pokemon,
    raw as 'raw usage', raw * 100 / sum(raw) as 'rawpcnt usage %',
    1500w as '1500 weighted', 1500w * 100 / sum(1500w) as '1500 weighted %',
    1630w as '1630 weighted', 1630w * 100 / sum(1630w) as '1630 weighted %',
    1760w as '1760 weighted', 1760w * 100 / sum(1760w) as '1760 weighted %'
FROM (
    SELECT
        mon.name AS pokemon,
        COUNT(*) AS raw,
        weighted_usage(pl.glicko, 1500) AS 1500w,
        weighted_usage(pl.glicko, 1630) AS 1630w,
        weighted_usage(pl.glicko, 1760) AS 1760w
    FROM mon_instance inst
    JOIN pokemon mon ON inst.pokemon_id = mon.pokemon_id
    JOIN battle ON battle.battle_id = inst.battle_id
    JOIN player_snapshot pl ON inst.player_snapshot_id = pl.player_snapshot_id
    JOIN metagame meta ON meta.metagame_id = battle.metagame_id
    WHERE meta.name = 'metagame'
    AND battle.start_date >= 'start month'
    AND battle.end_date < 'end month'
    GROUP BY mon.name
) data
ORDER BY 1630w DESC;
