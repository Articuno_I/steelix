// db.js
// Used for interfacing with the mongo db

'use strict';

// imports
const Pool = require('pg').Pool;
const config = require('./config_parser.js').config;

// determine whether the module is in a ready state. TODO: make this an event?
exports.loaded = false;

// Next, connect to the database
const pool = new Pool(config.db);  // TODO: handle errors in the above

// Load the static data tables into RAM to speed up later queries.
// I use objects of {name: id} because this is most useful.
exports.staticData = {};
exports.updateStaticData = function (tables, staticData) {
    if (!tables || tables === 'all')
        tables = ['pokemon', 'move', 'nature', 'ability', 'item', 'metagame'];
    else if (!tables || typeof(tables) === 'string')
        tables = [tables];

    for (let i in tables) {
        const table = tables[i];
        pool.query(`SELECT ${table}_id as id, name FROM ${table}`, (e, res) => {
            if (e) {
                throw new Error(`Error trying to read from table ${table}: ${e}`);
            }
            staticData[table] = {};
            for (let j in res.rows) {
                const row = res.rows[j];

                // a few sanity checks required by the rest of the codebase.
                if (table === 'metagame' && row.name.indexOf('-') != -1) {
                    // TODO: raise an error
                }

                staticData[table][row.id] = row.name;
            }
        });
    }

    exports.loaded = true;
};
exports.updateStaticData('all', exports.staticData);

exports.pool = pool;
