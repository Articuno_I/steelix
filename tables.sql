-- tables.sql - setup for the tables used in the Steelix Database

-- STATIC DATA TABLES
CREATE TABLE IF NOT EXISTS pokemon (
    pokemon_id INTEGER PRIMARY KEY,
    base_forme_id INTEGER REFERENCES pokemon(pokemon_id),
    dex_no INT,
    name TEXT,
    -- abilities JSON,
    -- movepool JSON
);

CREATE TABLE IF NOT EXISTS move (
    move_id INTEGER PRIMARY KEY,
    name TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS nature (
    nature_id INTEGER PRIMARY KEY,
    name TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS ability (
    ability_id INTEGER PRIMARY KEY,
    name TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS item (
    item_id INTEGER PRIMARY KEY,
    name TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS metagame (
    metagame_id SERIAL PRIMARY KEY,
    name TEXT UNIQUE
);

CREATE TABLE IF NOT EXISTS player_snapshot (
    player_snapshot_id BIGSERIAL PRIMARY KEY,
    player_id INTEGER NOT NULL REFERENCES player_lookup(player_id),
    metagame_id INTEGER REFERENCES metagame(metagame_id),
    game_count INTEGER,
    glicko1 FLOAT,
    glicko_dev FLOAT,
    gxe FLOAT, -- can be calc'd from glicko, but might as well store here
    elo FLOAT
);
-- TODO: partition this on month? Or maybe meta? Not sure this is needed.
CREATE INDEX IF NOT EXISTS player_meta_snapshot ON player_snapshot USING BTREE (
    metagame_id, player_id);

CREATE TABLE IF NOT EXISTS player_lookup (
    player_id SERIAL PRIMARY KEY,
    username TEXT
);
CREATE UNIQUE INDEX IF NOT EXISTS player_username ON player_lookup
    USING BTREE (username);

-- EV / IV spreads are separated from movesets due to being repeated so often
CREATE TABLE IF NOT EXISTS ev_spread (
    ev_spread_id SERIAL PRIMARY KEY,
    hp_evs SMALLINT (testing >=0 AND testing < 256),
    atk_evs SMALLINT (testing >=0 AND testing < 256),
    def_evs SMALLINT (testing >=0 AND testing < 256),
    spa_evs SMALLINT (testing >=0 AND testing < 256),
    spd_evs SMALLINT (testing >=0 AND testing < 256),
    spe_evs SMALLINT (testing >=0 AND testing < 256)
    /* TODO: consider not having these checks.
     * All data is coming from a trusted source (aka PS!), and these will
     * slow down insertions.
     * I also have to consider the possibility of some OM where this wouldn't
     * always be the case, I suppose?
     */
);
CREATE UNIQUE INDEX IF NOT EXISTS ev_spread_index ON ev_spread
    USING BTREE (hp_evs, atk_evs, def_evs, spa_evs, spd_evs, spe_evs);

CREATE TABLE IF NOT EXISTS iv_spread (
    iv_spread_id SERIAL PRIMARY KEY,
    hp_ivs SMALLINT (testing >=0 AND testing < 32),
    atk_ivs SMALLINT (testing >=0 AND testing < 32),
    def_ivs SMALLINT (testing >=0 AND testing < 32),
    spa_ivs SMALLINT (testing >=0 AND testing < 32),
    spd_ivs SMALLINT (testing >=0 AND testing < 32),
    spe_ivs SMALLINT (testing >=0 AND testing < 32)
);
CREATE UNIQUE INDEX IF NOT EXISTS iv_spread_index ON iv_spread
    USING BTREE (hp_ivs, atk_ivs, def_ivs, spa_ivs, spd_ivs, spe_ivs);

CREATE TABLE IF NOT EXISTS spread (
    spread_id BIGSERIAL PRIMARY KEY,
    ev_spread_id INTEGER NOT NULL REFERENCES ev_spread(ev_spread_id),
    iv_spread_id INTEGER NOT NULL REFERENCES iv_spread(iv_spread_id),
    nature_id INTEGER NOT NULL REFERENCES natures(nature_id)
);
CREATE UNIQUE INDEX IF NOT EXISTS spread_index ON spread
    USING BTREE (ev_spread_id, nature_id, iv_spread_id);

CREATE TABLE IF NOT EXISTS battle (
    battle_id BIGINT NOT NULL,    -- INT goes up to 2e9. We're already at 9e8.
    metagame_id INTEGER NOT NULL REFERENCES metagame(metagame_id),
    start_date DATE NOT NULL,
    player1_sn_id INTEGER NOT NULL REFERENCES player_snapshot.player_snapshot_id,
    player2_sn_id INTEGER NOT NULL REFERENCES player_snapshot.player_snapshot_id,
    average_glicko FLOAT NOT NULL,
    winner BOOLEAN,     -- NULL = tie
    win_type TEXT,      -- TODO: consider enumerating this?
    turn_count SMALLINT,
    CONSTRAINT month_meta_gameid PRIMARY KEY (start_date, metagame_id, game_id)
) PARTITION BY RANGE (start_date);

CREATE TABLE IF NOT EXISTS mon_instance (
    mon_instance_id BIGINT NOT NULL,
    battle_id BIGINT NOT NULL REFERENCES battle.game_id,
    start_date DATE NOT NULL,
    is_team1 BOOLEAN NOT NULL,
    player_snapshot_id BIGINT REFERENCES player_snapshot.player_snapshot_id,

    pokemon_id INTEGER NOT NULL REFERENCES pokemon(pokemon_id),
    moves INTEGER[],
    spread_id BIGINTEGER NOT NULL REFERENCES spread(spread_id),
    ability_id INTEGER NOT NULL REFERENCES ability(ability_id),
    item_id INTEGER NOT NULL REFERENCES item(item_id)
    level INTEGER NOT NULL DEFAULT 100,
    -- TODO: consider whether I need things like happiness, etc. here

    CONSTRAINT month_teamid PRIMARY KEY (start_date, mon_instance_id)
) PARTITION BY RANGE (start_date);
CREATE INDEX IF NOT EXISTS battle_team ON mon_instance USING HASH (battle_id);
