CREATE OR REPLACE FUNCTION weighting_c (glicko FLOAT, glicko_dev FLOAT, cutoff INTEGER)
RETURNS FLOAT
AS './weighting.c', 'weighting'
LANGUAGE C STRICT;


CREATE OR REPLACE FUNCTION weight_glicko (glicko FLOAT, glicko_dev FLOAT, cutoff INTEGER)
RETURNS FLOAT
AS $$
DECLARE
    glicko ALIAS FOR $1:
    glicko_dev ALIAS FOR $2;
    cutoff ALIAS FOR $3;
    weighting FLOAT;
BEGIN
    CASE
        WHEN glicko_dev < 100.0 AND weighting >= 1500 THEN weighting = 0.0;
        ELSE weighting = weighting_c(glicko, glicko_dev, cutoff);
    END;
    RETURN weighting
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE AGGREGATE weighted_usage (FLOAT, INTEGER)
(
    -- TODO: make sure this is finished / tested
    SFUNC = weight_glicko,
    STYPE = FLOAT,
    INITCOND = 0.0
)


CREATE OR REPLACE FUNCTION get_player_snapshot (
    username TEXT, metagame_id INTEGER, game_count INTEGER, 
    glicko1 FLOAT, glicko_dev FLOAT, elo FLOAT
) RETURNS BIGINTEGER
AS $$
DECLARE
    username ALIAS FOR $1;
    metagame_id ALIAS FOR $2;
    game_count ALIAS FOR $3;
    glicko1 ALIAS FOR $4;
    glicko_dev ALIAS FOR $5;
    elo ALIAS FOR $6;
    player_id BIGINTEGER;
    player_snapshot_id BIGINTEGER;
BEGIN
    SELECT player_id INTO id FROM (
        WITH s AS (
            SELECT player_id FROM player_lookup p
            WHERE username = p.username
        ), WITH i AS (
            INSERT INTO player_lookup (username)
            SELECT username
            WHERE NOT EXISTS (SELECT 1 FROM s)
            RETURNING player_id
        )
        SELECT player_id FROM s
        UNION ALL
        SELECT player_id FROM i
    ) tmp;

    SELECT player_snapshot_id INTO player_snapshot_id FROM (
        INSERT INTO player_snapshot (
            player_id, metagame_id, game_count, glicko1, glicko_dev, elo
        ) VALUES (
            id, metagame_id, game_count, glicko1, glicko_dev, elo
        ) RETURNING player_snapshot_id
    ) tmp;

    RETURN player_snapshot_id;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION get_spread_id (
    hp_iv SMALLINT,  atk_iv SMALLINT, def_iv SMALLINT,
    spa_iv SMALLINT, spd_iv SMALLINT, spe_iv SMALLINT,
    hp_ev SMALLINT,  atk_ev SMALLINT, def_ev SMALLINT,
    spa_ev SMALLINT, spd_ev SMALLINT, spe_ev SMALLINT,
    nature_id INTEGER
)
RETURNS BIGINTEGER
AS $$
DECLARE
    hp_iv ALIAS FOR $1;
    atk_iv ALIAS FOR $2;
    def_iv ALIAS FOR $3;
    spa_iv ALIAS FOR $4;
    spd_iv ALIAS FOR $5;
    spe_iv ALIAS FOR $6;
    hp_ev ALIAS FOR $7;
    atk_ev ALIAS FOR $8;
    def_ev ALIAS FOR $9;
    spa_ev ALIAS FOR $10;
    spd_ev ALIAS FOR $11;
    spe_ev ALIAS FOR $12;
    nature_id ALIAS FOR $13;
    iv_sp_id INTEGER;
    ev_sp_id INTEGER;
    sp_id BIGINTEGER;
BEGIN
    SELECT iv_spread_id INTO iv_sp_id FROM (
        WITH s AS (
            SELECT iv_spread_id FROM iv_spread
            WHERE hp_iv = hp_ivs
            AND atk_iv = atk_ivs
            AND def_iv = def_ivs
            AND spa_iv = spa_ivs
            AND spd_iv = spd_ivs
            AND spe_iv = spe_ivs
        ), WITH i AS (
            INSERT INTO iv_spread (
                hp_ivs, atk_ivs, def_ivs, spa_ivs, spd_ivs, spe_ivs
            ) SELECT hp_iv, atk_iv, def_iv, spa_iv, spd_iv, spe_iv
            WHERE NOT EXISTS (SELECT 1 FROM s)
            RETURNING iv_spread_id
        )
        SELECT iv_spread_id FROM s
        UNION ALL
        SELECT iv_spread_id FROM i
    ) tmp;

    SELECT ev_spread_id INTO ev_sp_id FROM (
        WITH s AS (
            SELECT ev_spread_id FROM ev_spread
            WHERE hp_ev = hp_evs
            AND atk_ev = atk_evs
            AND def_ev = def_evs
            AND spa_ev = spa_evs
            AND spd_ev = spd_evs
            AND spe_ev = spe_evs
        ), WITH i AS (
            INSERT INTO ev_spread (
                hp_evs, atk_evs, def_evs, spa_evs, spd_evs, spe_evs
            ) SELECT hp_ev, atk_ev, def_ev, spa_ev, spd_ev, spe_ev
            WHERE NOT EXISTS (SELECT 1 FROM s)
            RETURNING ev_spread_id
        )
        SELECT ev_spread_id FROM s
        UNION ALL
        SELECT ev_spread_id FROM i
    ) tmp;

    SELECT spread_id INTO sp_id FROM (
        INSERT INTO spread (ev_spread_id, iv_spread_id, nature_id)
        VALUES (ev_sp_id, iv_sp_id, nature_id)
        RETURNING spread_id
    ) tmp;

    RETURN sp_id
END;
$$ LANGUAGE plpgsql;
