// config_parser.js

'use strict';

// imports
const yaml = require('js-yaml');
const fs = require('fs');

// Filename constants
const CONFFILE = './config/config.yaml';
const CONFEXAMPLE = './config/config-example.yaml';

// Find the config file to use
let confFile = CONFFILE;
if (!fs.existsSync(confFile)) {
    if (fs.existsSync(CONFEXAMPLE)) {
        console.log(`Unable to find file "${CONFFILE}". `
            + `Using "${CONFEXAMPLE}" instead.`);
        confFile = CONFEXAMPLE;
    } else {
        throw new Error('Unable to find any config files.');
    }
}

let config;
try {
    config = yaml.load(fs.readFileSync(confFile, 'utf-8'));
} catch (e) {
    throw new Error(`Error trying to load file "${confFile}": ${e}`);
}

exports.config = config;
