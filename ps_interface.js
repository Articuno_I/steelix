// ps_interface.js
// Takes data from PS! and puts it into the db.

const db = require('./db.js');

exports.reloadStaticData = function (tables) {
    db.updateStaticData(tables, db.staticData);
};

exports.insertGame = async function (battleName, startTime, p1, p2, winner,
    winType, turnCount, p1Team, p2Team, retries) {
    /*
     * Inputs:
     * battleName: string like "ou-12345"
     * p1, p2: objects with username, gameCount, glicko1, glickoDev, elo
     * startTime, winner, winType: TODO
     * p1Team, p2Team: arrays of mon objects with these keys:
     *     nature: string
     *     item: string
     *     moves: array of strings (or objects? Not sure yet)
     *     evs, ivs: arrays
     *     nature: string
     *     level: integer
     * retries: number of times to attempt to reconnect to the db on failure
     */
    let client;
    if (!db.loaded) {
        // TODO: log a warning here
        if (retries) {
            setTimeout(module.exports.insertGame, 5000, battleName, p1, p2,
                winner, winType, turnCount, p1Team, p2Team, --retries);
        } else {
            // TODO: handle
        }
        return null;
    } else {
        try {
            client = await db.pool.connect();
        } catch (err) {
            // TODO: handle this
            return null;
        }
    }

    // parse inputs to a format pg will understand
    // battleName is of the form "ou-12345".
    const battleIdParts = battleName.split('-');
    const metagameId = db.staticData.metagame[battleIdParts[0]];
    const battleId = battleIdParts[1];
    // TODO: modify startTime so pg will understand it?

    // Insertions
    // First, the two player_snapshot entries
    let [p1SnId, p2SnId] = await Promise.all(
        client.query(`get_player_snapshot(
            ${p1.username}, ${metagameId}, ${p1.gameCount},
            ${p1.glicko1}, ${p1.glickoDev}, ${p1.elo}
        )`),
        client.query(`get_player_snapshot(
            ${p2.username}, ${metagameId}, ${p2.gameCount},
            ${p2.glicko1}, ${p2.glickoDev}, ${p2.elo}
        )`)
    );

    // Then, set up other insertions, which can all happen simultaneously.
    // Battle table entry
    let queries = [client.query(`
        INSERT INTO battle (
            battle_id, metagame_id, start_date,
            player1_sn_id, player2_sn_id, winner, win_type, turn_count
        ) VALUES (
            ${battleId}, ${metagameId}, ${startTime},
            ${p1SnId}, ${p2SnId}, winner, winType, turnCount
        );
    `)];

    // Insert Team data
    function insertMon(mon, battleId, startTime, pSnId, isP1) {
        const monId = db.staticData.pokemon[mon.name];
        let moves = [];
        for (let j in mon.moves) {
            moves.push(db.staticData.move[mon.moves[j]]);  // `mon.moves[j].name`?
        }
        moves = '{' + moves.sort() + '}';
        const natureId = db.staticData.nature[mon.nature];
        const itemId = db.staticData.item[mon.item];

        return client.query(`
            INSERT INTO mon_instance (
                battle_id, start_date, is_team1, player_snapshot_id, pokemon_id,
                moves, spread_id, item_id, level
            ) VALUES (
                ${battleId}, ${startTime}, ${isP1}, ${pSnId}, ${monId},
                ${moves}, get_spread_id(
                    ${mon.ivs.join(',')}, ${mon.evs.join(',')}, ${natureId}
                ), ${itemId}, ${mon.level}
            )
        `);
    }
    for (let i in p1Team) {
        queries.push(insertMon(p1Team[i], battleId, startTime, p1SnId, true));
    }
    for (let i in p2Team) {
        queries.push(insertMon(p2Team[i], battleId, startTime, p2SnId, false));
    }

    await Promise.all(queries);
    client.release();
};
