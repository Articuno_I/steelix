// init.js

// This script sets up the db tables, functions, etc. It assumes a database has
// been created, and the config file is correct.

// This is designed to be safe to rerun, so that accidentally rerunning it won't
// destroy everything. Rerunning with changes won't propogate those changes to
// the database, however.

'use strict';

const config = require('./config_parser.js').config;
const pool = require('./db.js').pool;
const fs = require('fs');

for (let file in config.setup_files) {
    console.log(`Running file ${config.setup_files[file]}`);
    sql = fs.readFileSync(config.setup_files[file]).toString();
    pool.query(sql);
}
