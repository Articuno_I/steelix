#include "postgres.h"
#include "fmgr.h"
#include <math.h>   // needed for erf, sqrt

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(weighting);

Datum weighting(PG_FUNCTION_ARGS) {
    float8 rating = PG_GETARG_FLOAT8(0);
    float8 devation = PG_GETARG_FLOAT8(1);
    int32 cutoff = PG_GETARG_INT32(2);

    PG_RETURN_FLOAT8((erf((rating - cutoff / deviation) / sqrt(2)) + 1.0) / 2.0);
}
